import ora from 'ora';
import chalk from 'chalk';
import fs from 'fs-extra';
import path from 'path';
import { exec } from 'child_process';
import util from 'util';

const execAsync = util.promisify(exec);

function createGitFiles(options: ProjectOptions) {
	const content = ['node_modules', 'foundryconfig.json'];

	if (options.useTypeScript && options.cssPrep) {
		content.push('dist');
	} else if (options.useTypeScript) {
		content.push('dist/module', 'dist/templates', 'dist/**/*.js', 'dist/**/*.json');
	} else if (options.cssPrep) {
		content.push('dist/fonts', 'dist/assets', 'dist/**/*.css');
	}

	fs.writeFileSync(path.resolve(options.dir, '.gitignore'), content.join('\n'), 'utf8');

	// Folders that are empty by default should be committed
	const emptyDirs = ['fonts', 'styles', 'templates', 'assets'];
	const distDir = path.resolve(options.dir, 'dist');
	const srcDir = path.resolve(options.dir, 'src');
	for (const dir of emptyDirs) {
		if (fs.existsSync(path.join(distDir, dir))) {
			fs.createFileSync(path.join(distDir, dir, '.gitkeep'));
		} else if (fs.existsSync(path.join(srcDir, dir))) {
			fs.createFileSync(path.join(srcDir, dir, '.gitkeep'));
		}
	}
}

export default async (options: ProjectOptions) => {
	return new Promise(async (resolve, reject) => {
		if (options.git) {
			const spinner = ora('Initializing new Git repository').start();

			await execAsync('git rev-parse --show-toplevel', { cwd: options.dir })
				.then(() => {
					createGitFiles(options);
					spinner.succeed(chalk.green('Existing repository detected. Created .gitignore'));
				})
				.catch(async (out) => {
					if (out.stderr) {
						await execAsync('git init', { cwd: options.dir })
							.then(async (initOut) => {
								if (initOut.stderr) {
									spinner.fail(chalk.red('Failed to initialize Git repository'));
									reject(initOut.stderr);
								} else {
									createGitFiles(options);
									spinner.succeed(chalk.green('Initialized new Git repository'));
								}
							});
					}
				});
		}
		resolve();
	});
};
