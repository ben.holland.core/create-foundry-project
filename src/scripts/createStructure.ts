import ora from 'ora';
import path from 'path';
import fs from 'fs-extra';
import chalk from 'chalk';
import stringify from 'json-stringify-pretty-compact';

import Rename from '../util/rename';

const replace = require('replace');

/**
 * Creates required source folders
 * @param cssPrep Which CSS preprocessor to use, if any
 */
async function createSourceFolders(options: ProjectOptions) {
	const baseDirs: string[] = ['dist'];

	// If using TypeScript, create source folder
	// if not, we don't need to build code
	if (options.useTypeScript) {
		baseDirs.push('src/module', 'src/templates');
	} else {
		baseDirs.push('dist/module', 'dist/templates');
	}

	// If a preprocessor was chosen, add a 'styles' folder,
	// if not, we don't need to build styles
	if (options.cssPrep) {
		baseDirs.push('src/styles', 'src/fonts', 'src/assets');
	} else {
		baseDirs.push('dist/fonts', 'dist/assets');
	}

	for (const srcDir of baseDirs) {
		await fs.mkdirs(path.resolve(options.dir, srcDir));
	}
}

/**
 * Creates required JSON files
 */
async function createJson(options: ProjectOptions) {
	const filename = options.isSystem ? 'system' : 'module';
	const sourceDir = options.useTypeScript ? 'src' : 'dist';

	const projectJson = {
		name: options.name,
		title: options.name,
		description: '',
		systems: [],
		version: '0.1.0',
		author: '',
		authors: [{
			name: '',
			email: '',
			url: ''
		}],
		scripts: [],
		esmodules: [`${options.name}.js`],
		styles: [`${options.name}.css`],
		packs: [],
		languages: [],
		gridDistance: 1,
		gridUnits: '',
		minimumCoreVersion: '0.5.0',
		compatibleCoreVersion: '0.5.0',
		url: '',
		manifest: '',
		download: '',
		license: '',
		readme: '',
		bugs: '',
		changelog: ''
	};

	const templateJson = {
		Actor: {
			types: [],
			templates: []
		},
		Item: {
			types: [],
			templates: []
		}
	};

	if (!options.isSystem) {
		delete projectJson.gridDistance;
		delete projectJson.gridUnits;
	} else {
		delete projectJson.systems;
	}

	const prettyProjectJson = stringify(projectJson, { maxLength: 35 });

	await fs.writeFile(path.resolve(options.dir, sourceDir, `${filename}.json`), prettyProjectJson, 'utf8');
	if (options.isSystem) await fs.writeJSON(path.resolve(options.dir, sourceDir, 'template.json'), templateJson, { spaces: 2 });
}

/**
 * Creates JavaScript or TypeScript entry and boilerplate files
 * TODO: Add separate TypeScript template
 */
async function createSource(options: ProjectOptions) {
	const templateDir = path.resolve(__dirname, '../..', 'template/code', 'js');
	const sourceDir = options.useTypeScript ? 'src' : 'dist';
	const dir = path.resolve(options.dir, sourceDir);

	// Copy the source files
	await fs.copy(templateDir, dir, { recursive: true });

	// Rename entry file to match project name
	await fs.rename(path.resolve(dir, 'sourceEntry.js'), path.resolve(dir, `${options.name}.js`));

	// Define variable substitutions
	const replacements = {
		LANG: options.useTypeScript ? 'TypeScript' : 'JavaScript',
		TYPE: options.isSystem ? 'system' : 'module',
		NAME: options.name,
		SHORTTITLE: options.name,
		TITLE: options.name
	};

	// Replace variables in boilerplate
	for (const [key, value] of Object.entries(replacements)) {
		replace({
			regex: `<${key}>`,
			replacement: value,
			paths: [dir],
			recursive: true,
			silent: true
		});
	}

	// Rename JavaScript files to TypeScript
	if (options.useTypeScript) {
		const rename = new Rename('js', 'ts', dir);
		rename.traverse();
	}
}

/**
 * Creates CSS or Less/Sass source files
 */
async function createStyles(options: ProjectOptions) {
	const templateDir = path.resolve(__dirname, '../..', 'template/code', 'css');
	const sourceDir = options.cssPrep ? 'src' : 'dist';
	const dir = path.resolve(options.dir, sourceDir);
	let cssExt = '.css';

	// If a CSS preprocessor is chosen, assign the correct extension
	if (options.cssPrep) {
		switch (options.cssPrep) {
			case 'less':
				cssExt = '.less';
				break;
			case 'sass':
				cssExt = '.scss';
				break;
			default:
				throw new Error(`Incorrect parameter for CSS preprocessor. Expected "less" or "sass". Got: "${options.cssPrep}"`);
		}
	}

	// Copy the source files
	await fs.copy(templateDir, dir, { recursive: true });

	// Define variable substitutions
	// Convert commandline argument to title case
	const prepTitle = options.cssPrep ? [
		options.cssPrep[0].toUpperCase(),
		options.cssPrep.substring(1).toLowerCase()].join('') : 'CSS';
	const replacements = {
		CSS_LANG: prepTitle
	};

	// Replace variables in boilerplate
	for (const [key, value] of Object.entries(replacements)) {
		replace({
			regex: `<${key}>`,
			replacement: value,
			paths: [dir],
			recursive: true,
			silent: true
		});
	}

	// Ensure the right CSS files exist
	if (options.cssPrep) {
		await fs.remove(path.resolve(dir, 'style.css'));
		await fs.rename(path.resolve(dir, 'styleEntry.less'), path.resolve(dir, options.name + cssExt));
	} else {
		await fs.remove(path.resolve(dir, 'styleEntry.less'));
		await fs.rename(path.resolve(dir, 'style.css'), path.resolve(dir, options.name + cssExt));
	}
}

export default async (options: ProjectOptions) => {
	const spinner = ora('Creating project structure').start();

	try {
		await createSourceFolders(options);
		await createJson(options);
		await createSource(options);
		await createStyles(options);
	} catch (err) {
		spinner.fail(chalk.red('Failed to create project structure'));
		throw chalk.red(err);
	}

	spinner.succeed(chalk.green('Created project structure'));
};
