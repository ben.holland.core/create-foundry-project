import chalk from 'chalk';
import logSymbols from 'log-symbols';
import { spawn } from 'cross-spawn';

export default async (options: ProjectOptions): Promise<void> => {
	const deps: string[] = [
		'typescript',
		'fs-extra',
		'chalk',
		'archiver',
		'yargs',
		'sass',
		'json-stringify-pretty-compact',
		'gulp',
		'gulp-typescript',
		'gulp-less',
		'gulp-sass',
		'gulp-git',
		'gitlab:foundry-projects/foundry-pc/foundry-pc-types'
	];

	return new Promise((resolve, reject) => {
		if (deps.length > 0 && options.deps) {
			const command = 'npm';
			const args = [
				'install',
				'--loglevel',
				'error',
				'--save-dev'
			].concat(deps);

			console.log(logSymbols.info, 'Installing dependencies:');
			console.log(chalk.blueBright('   ', deps.join('\n    ')));

			const child = spawn(command, args, { stdio: 'inherit', cwd: options.dir });

			child.on('close', (code) => {
				if (code !== 0) {
					console.error(chalk.red(logSymbols.error, 'Error installing dependencies'));
					reject(new Error(`Child process exited with code ${code}`));
					return;
				}
				console.log(chalk.green(logSymbols.success, 'Installed dependencies'));
				resolve();
			});
		} else {
			resolve();
		}
	});
};
