**DEPRECATED:** Unfortunately, due to prioritization of more important work, I hade to make the decision to deprecate the Project Creator. It will remain as-is, with no further updates from me. Anyone is free to fork it and work on it themselves if they want to. In the near future, I may create a detailed guide on how to set up a development environment for those new to Foundry development. It hasn't been an easy decision as I know quite a few use it for their projects, but as my other projects take priority and maintaining and overhauling the Project Creator is taking too much time away from that, I had to make it. The type definitions package is excluded from this decision and I will continue to maintain it.

# Foundry Project Creator
The **Foundry Project Creator** is a tool that developers can use to create modules and systems for the Foundry Virtual Tabletop software. It is designed to provide a (partially opinionated) boilerplate project and a set of scripts to quickly get started.

The CLI only creates the boilerplate project. It will by default install several development dependencies in order to use Gulp, which automates the most frequently-used tasks. When using TypeScript, it will also install the following:

* **[Type definitions](https://gitlab.com/foundry-projects/foundry-pc/foundry-pc-types):** Contains type definitions for the Foundry API, useful for autocomplete and Intellisense in both vanilla JavaScript and TypeScript.

**Note:** The above package is not published to the npm registry but is installed from its repository directly. As such, it can only be updated by reinstalling the package directly. In projects created with the CLI, run `npm run update` to quickly update the type definitions. You should remove this script if you uninstall the package, or add it if you install it manually.

See the [wiki](https://gitlab.com/foundry-projects/foundry-pc/create-foundry-project/-/wikis/home) for more information on what the CLI does.

## Usage
You can easily use the tool without installing by executing the following:
```
npx create-foundry-project <project-directory> [options]
```
If you cannot use `npx`, the classic method is to install the package globally
```
npm install -g create-foundry-project
```
and then execute it:
```
foundry <project-directory> [options]
```

### Options
The tool accepts the following options:
* `-s, --system`: Creates a system rather than a module.
* `-t, --typescript`: Configures the project to use TypeScript instead of regular JavaScript.
* `--css <preprocessor>`: Configures the project to use a CSS preprocessor (`less` or `sass`).
* `-f, --force`: Forces the tool to clean and use an existing directory (WARNING: all files inside the directory will be deleted).
* `--no-deps`: Skip installing the dependencies. Useful if you prefer to handle these things yourself. This will also not set up Gulp.
* `--no-git`: Skip initializing a Git repository.

### Setup
Once set up, there may be a few things you might want to check. Specifically, the manifest file (either `system.json` or `module.json`) contains a set of properties that Foundry uses. When using the CLI, it adds all valid properties for systems or modules, and you should remove any you don't use. Check the Foundry documentation on [module development](https://foundryvtt.com/article/module-development/) for information about each property.

## Gulp
Projects created with the CLI will by default include a `gulpfile` as well as any dev dependency required by it. These scripts have replaced a separate scripts package to automate a number of tasks:
* **Build:** To build TypeScript, Less and/or SASS, and copy JSON, fonts, and templates to the `dist` folder.
* **Link:** To create a symbolic link to the build in the configured User Data folder for testing in Foundry.
* **Watch:** To watch for any changes and rebuild the module.
* **Clean:** To quickly clean the `dist` folder.
* **Package:** To quickly package the build for testing or manual distribution.
* **Publish:** To simplify releasing a module by updating the manifest, packaging, and tagging a release.

See the [wiki](https://gitlab.com/foundry-projects/foundry-pc/create-foundry-project/-/wikis/home) for more information about how to use it.

Even if you are not using the CLI, you may find these tasks useful. You can copy the `gulpfile`, or derive from it, and use it in your own projects if you wish. Make sure you install the dev dependencies by running the following:
```
npm install --save-dev node-sass typescript fs-extra chalk archiver yargs json-stringify-pretty-compact gulp gulp-typescript gulp-less gulp-sass gulp-git
```