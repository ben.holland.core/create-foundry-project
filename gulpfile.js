const fs = require('fs-extra');
const gulp = require('gulp');
const eslint = require('gulp-eslint');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const ts = require('gulp-typescript');
const tsProj = ts.createProject('tsconfig.json');
const scripts = ['src/**/*.ts'];

function clean() {
	return fs.remove('dist');
}

function lint() {
	return gulp.src(scripts)
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError())
}

function compileScripts() {
	clean();
	
	return gulp.src(scripts)
		.pipe(sourcemaps.init())
		.pipe(tsProj())
		.pipe(babel({filename: 'babel.config.json'}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('dist'));
}

function watch() {
	gulp.watch(scripts, compileScripts);
}

exports.lint = lint;
exports.watch = watch;
exports.build = compileScripts;
exports.clean = clean;
exports.default = gulp.series(lint, compileScripts);